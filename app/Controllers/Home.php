<?php
namespace App\Controllers;

use App\Models\User;

class Home
{
	public $user;

	public function __construct(User $user)
	{
		$this->user = $user;
	}


	public function index()
	{
		$data = ['users' => ['name' => 'jeff', 'name' => 'pon', 'name' => 'belle']];
		
		view('home', $data);
	}
}
