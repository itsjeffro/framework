<?php
namespace App\Models;

use Jeff\Database\Database;

class User
{
	public $db;

	public function __construct()
	{
		$this->db = new Database;
	}


	public function fetch()
	{
		$results = [];

		$stmt = $this->db->mysqli->prepare("SELECT id, name, email FROM users");
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($id, $name, $email);

		while($stmt->fetch()) {
			$results[] = (object) ['id' => $id, 'name' => $name, 'email' => $email];
		}

		return $results;
	}


	public function insert()
	{

	}


	public function update()
	{
		
	}


	public function delete()
	{
		
	}
	
}
