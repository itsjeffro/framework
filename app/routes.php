<?php
$routes->get('/', 'Home@index');

$routes->get('about', 'About@index');

$routes->get('test/{test}', function($test) {
	echo $test;
});