<!doctype html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>php framework</title>

	<link rel="stylesheet" href="assets/css/framework.css">
	<link rel="stylesheet" href="assets/css/site.css">
</head>
<body>
	
	<header>
		<div class="container">
			<div class="nav">
				<ul>
					<li><a href="/">Home</a></li>
					<li><a href="/about">About</a></li>
					<li><a href="/test/test">Test page</a></li>
				</ul>
			</div>

			<h3>Another PHP framework</h3>
		</div>
	</header>