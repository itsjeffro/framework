<?php
// Determin enviroment and dateTime zone
require_once 'settings.php';


// Bring in the composer autoloader
require_once __DIR__ . '/vendor/autoload.php';


// Some helpers for the site
require_once 'vendor/itsjeffro/jeff/src/Jeff/Helpers/helpers.php';


// Get Routes
$routes = new Jeff\Routing\Router;

require_once APP_PATH.'/routes.php';

$routes->dispatch();