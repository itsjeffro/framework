<?php
define('APP_PATH', 'app');
define('ENVIROMENT', 'development');

switch(ENVIROMENT) {
	case 'development':
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(-1);
		break;

	case 'production':
	default:
		error_reporting(0);
		break;
}

// Timezone
date_default_timezone_set('Australia/Sydney');

// Start session to allow login and such
session_start();
?>